#ifndef PLAYER_H
#define PLAYER_H

#include <string>

using std::string;

/**
 * \brief   This class holds information about a player.
 *
 *          A player always has a name and a symbol (char) associated with the player.
 *          The player's markers on a game board can be identified via this associated marker symbol.
 *
 *          The player's symbol should be unique for each player in a match in order to avoid confusion.
 */
class Player
{
    public:
        Player(string name, char symbol);

        string getName();
        char getSymbol();

    private:
        string _name;
        char _symbol;
};

#endif // PLAYER_H
