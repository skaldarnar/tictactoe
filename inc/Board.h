#ifndef BOARD_H
#define BOARD_H

#include <string>

using std::string;

class Board
{
    public:
        Board(int width, int height);
        virtual ~Board();

        void draw();

        int getWidth();
        int getHeight();

        char getChar(int row, int col);
        char& getCurrentSymbol();
        void setChar(int row, int col, char c);

        void moveCursor(int row, int col);
        void moveUp();
        void moveDown();
        void moveLeft();
        void moveRight();

        char& operator()(int row, int col);
    private:
        int _width;
        int _height;
        int _curs_r = 0, _curs_c = 0;
        char **_board;

        /**
         * \brief   An enumeration of 9 relative positions around a pixel in a grid.
         *
         *          The enumeration entries describe the relative position in the neighborhood
         *          of a given position, like so:
         *
         *              tl | tc | tr
         *              ------------
         *              cl | cc | cr
         *              ------------
         *              bl | bc | br
         */
        enum position {
            tl, /**< TOP-LEFT */
            tc, /**< TOP-CENTER */
            tr, /**< TOP-RIGHT */
            cl, /**< CENTER-LEFT */
            cc, /**< CENTER-CENTER */
            cr, /**< CENTER-RIGHT */
            bl, /**< BOTTOM-LEFT */
            bc, /**< BOTTOM-CENTER */
            br  /**< BOTTOM-RIGTH */
        };

        string getBoxCharacter(int i, int j);
        string getCursorBorder(int i, int j, position p);
        void drawFrame(int center_row, int center_col);
        void drawMarkers(int center_row, int center_col);
        void drawCursor(int center_row, int center_col);
};

#endif // BOARD_H
