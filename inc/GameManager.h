#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include "Board.h"
#include "Player.h"

#include <vector>

/**
 * \brief   The basic game manager class which handles the game initialization and processing.
 *
 *          INITALIZATION:
 *          The game manager will initialize a new game with asking the user for at least two player
 *          names and symbols. The user is free to choose as many competitors as s/he wants.
 *          The manager will also ask the user to specify the game board dimensions (width x height)
 *          and the number of marks required to win.
 *
 *          GAME LOOP:
 *          The game loop lets each player make his/her move in turn. The move is verified (e.g., the
 *          selected cell is empty) and checked against the victory condition.
 *          The game proceeds with the next player if the victory condition is not met, otherwise a
 *          final screen is presented to the players.
 *          The game ends with a tie if all cells are marked but no player has won.
 */
class GameManager
{
    public:
        GameManager();

        void startGame();

    private:
        Board* board = {};
        std::vector<Player*> players;
        int currentPlayer = 0;
        int marksToWin;
        bool isRunning, turnFinished;

        Player* createPlayer();
        bool checkSymbol(char s);
        bool checkSize(int s);
        bool checkWinningCondition(int w, int x0, int x1);

        void initializePlayers();
        void initializeBoard();

        bool placeMark();

        void processInput();
        bool evaluateTurn();

        bool checkHorizontal(int r, int c, char s);
        bool checkVertical(int r, int c, char s);
        bool checkDiagonal(int r, int c, char s);
};

#endif // GAMEMANAGER_H
