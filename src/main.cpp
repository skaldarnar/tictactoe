#include <iostream>
#include <ncurses.h>

#include "GameManager.h"

using namespace std;

void init();
void shutdown();

int main()
{
    init();

    GameManager manager;
    manager.startGame();

    shutdown();

    return 0;
}

void init() {
    setlocale(LC_ALL, "");
    // initialize ncurses environment
    initscr();
    savetty();

    start_color();
    init_pair(1, COLOR_GREEN, COLOR_BLACK);

    noecho();
    keypad(stdscr, TRUE);
    cbreak();
    curs_set(0);
    clear();
}

void shutdown() {
    getch();

    resetty();
    endwin();
}
