#include "GameManager.h"

#include <ncurses.h>
#include <unistd.h>

using std::min;
using std::max;
using std::vector;

GameManager::GameManager() : isRunning(false) { }

/**
 * \brief   Initializes a new game and starts it.
 *
 *          Initializes the competing players and the board size with
 *          user input.
 *          Afterwards, the game loop is started.
 *
 */
void GameManager::startGame() {
    int cr, cc;
    int moves = 0;

    // initialization phase for a new game
    initializePlayers();
    initializeBoard();

    isRunning = true;
    while (isRunning) {
        getmaxyx(stdscr, cr, cc);
        cr /= 2;
        cc /= 2;

        turnFinished = false;

        // loops until the player has placed his mark or quit the game
        while(!turnFinished) {
            processInput();
            board->draw();
            mvprintw(1, cc, "Player: %s", players[currentPlayer]->getName().c_str());
            clrtoeol();
            refresh();

            usleep(30000);
        }

        // evaluates the current player's turn
        if (evaluateTurn()) {
            clear();
            string msg = "Player " + players[currentPlayer]->getName() + " has won!";
            mvprintw(cr, cc-msg.size()/2, msg.c_str());
            refresh();

            isRunning = false;
            getch();
        }

        moves++;
        // check for a tie
        // the game is tied if all cells are filled, but the current player has not won
        if (moves == board->getWidth() * board->getHeight()) {
            clear();
            string msg = "Tie!";
            mvprintw(cr, cc-msg.size()/2, msg.c_str());
            refresh();

            isRunning = false;
            getch();
        }

        currentPlayer = (currentPlayer+1) % players.size();
    }
}

/**
 * \brief   Initializes at least 2 players.
 */
void GameManager::initializePlayers() {
    int ch;
    for (;;) {
        players.push_back(createPlayer());
        if (players.size() >= 2) {  // add at least 2 players
            attron(A_BOLD);
            mvprintw(10, 2, "Do you want to add another player? (y/n)");
            attroff(A_BOLD);
            if ((ch = getch()) != ERR && (ch == 'n' || ch == 'N'))
                break;
        }
    }
}

/**
 * \brief   Initializes the game board and winning condition.
 *
 *          Initializes the game board and winning condition with user input.
 *          The board has to be at least 3x3 cells, and the number of marks
 *          required to win the game has to be between min(_width, _height)
 *          and max(_width, _height).
 */
void GameManager::initializeBoard() {
    char str[80];
    int width, height;

    clear();
    curs_set(1);
    echo();

    do {
        mvprintw(3, 2, "How wide would you like the board to be?");
        move(4,4);
        clrtoeol();
        getstr(str);
        width = atoi(str);
    } while (!checkSize(width));

    do {
        mvprintw(5, 2, "How high would you like the board to be?");
        move(6,4);
        clrtoeol();
        getstr(str);
        height = atoi(str);
    } while (!checkSize(height));

    do {
        mvprintw(7,2, "How many marks should be needed to win?");
        move(8,4);
        clrtoeol();
        getstr(str);
        marksToWin = atoi(str);
    } while (!checkWinningCondition(marksToWin, width, height));

    refresh();
    noecho();
    curs_set(0);

    board = new Board(width, height);
    clear();
    board->draw();
}

/**
 * \brief   Creates a new player with a unique symbol.
 *
 *          The user is asked to enter a player name and the player's symbol.
 *          The symbol must not be white space or a symbol already chosen by
 *          another player.
 */
Player* GameManager::createPlayer() {
    char name[80];
    char symbol;
    int numPlayers = players.size();

    clear();
    mvprintw(3, 2, "Welcome Player %d! What is your name?", numPlayers+1);
    move(4,4);
    curs_set(1);
    echo();
    getstr(name);

    do {
        mvprintw(5, 2, "What would you like you symbol to be?");
        move(6,4);
        clrtoeol();
        curs_set(1);
        echo();
        symbol = getch();
        noecho();
        curs_set(0);
    } while (!checkSymbol(symbol));

    mvprintw(9, 2, "Welcome %s (%c), press any key to continue.", name, symbol);
    refresh();
    getch();

    return new Player(string(name), symbol);
}

/**
 * \brief   Verifies that the symbol \a s is valid.
 *
 *          The character \a is a valid symbol if it is not a white space
 *          and the symbol is not already chosen by another player.
 *
 * \param   s   the character to check
 *
 * \return  true if the character is valid, false otherwise
 */
bool GameManager::checkSymbol(char s) {
    if (s == ' ' || s == '\n' || s == '\t')
        return false;

    vector<Player*>::iterator iter;
    for (iter = players.begin(); iter != players.end(); ++iter) {
        if ((*iter)->getSymbol() == s)
            return false;
    }

    return true;
}

/**
 * \brief   Verifies that the number \a s is a valid size.
 *
 *          The number \a is a valid size if it is at least 3.
 *
 * \param   s   the number to check
 *
 * \return  true if the number is valid, false otherwise
 */
bool GameManager::checkSize(int s) {
    return s > 2;
}

/**
 * \brief   Verifies that the number \a s is a valid winning condition.
 *
 *          The winning condition defines how many marks a player needs to have in row, column, or diagonal in
 *          order to win the game. The number \a is a valid winning condition if it is at least 3 and if it is
 *          smaller or equal to min(_height, _width).
 *
 * \param   s   the number to check
 *
 * \return  true if the number is valid, false otherwise
 */
bool GameManager::checkWinningCondition(int w, int x0, int x1) {
    return (w > 2) && (w <= min(x0, x1));
}

/**
 * \brief   Evaluates the current player's turn.
 *
 *          Checks all pieces of the current player on the board against the winning condition.
 *
 * \return  true if the current player has won, false otherwise
 */
bool GameManager::evaluateTurn() {
    int i, j;
    bool won;
    char s = players[currentPlayer]->getSymbol();
    for (i = 0; i < board->getHeight(); ++i) {
        for (j = 0; j < board->getWidth(); ++j) {
            if (board->getChar(i,j) == s) {
                won = won || checkHorizontal(i,j,s) || checkVertical(i,j,s) || checkDiagonal(i,j,s);
            }
        }
    }
    return won;
}

bool GameManager::checkHorizontal(int i, int j, char s) {
    int marks = 0;
    for (int c = j; c >= max(j-marksToWin, 0); --c) {
        if (board->getChar(i, c) == s)
            marks++;
        else
            break;
    }
    for (int c = j+1; c < min(j+marksToWin, board->getWidth()); ++c) {
        if (board->getChar(i, c) == s)
            marks++;
        else
            break;
    }
    return marks >= marksToWin;
}

bool GameManager::checkVertical(int i, int j, char s) {
    int marks = 0;
    for (int r = i; r >= max(i-marksToWin, 0); --r) {
        if (board->getChar(r, j) == s)
            marks++;
        else
            break;
    }
    for (int r = i+1; r < min(i+marksToWin, board->getHeight()); ++r) {
        if (board->getChar(r, j) == s)
            marks++;
        else
            break;
    }
    return marks >= marksToWin;
}

bool GameManager::checkDiagonal(int i, int j, char s) {
    int marks = 0;
    for (int d = 0; d < marksToWin; ++d) {
        if (i+d < board->getHeight() && j+d < board->getWidth() && board->getChar(i+d, j+d) == s)
            marks++;
        else
            break;
    }
    for (int d = 1; d < marksToWin; ++d) {
        if (i-d >= 0 && j-d >= 0 && board->getChar(i-d, j-d) == s)
            marks++;
        else
            break;
    }
    if (marks >= marksToWin)
        return true;
    marks = 0;

    for (int d = 0; d < marksToWin; ++d) {
        if (i+d < board->getHeight() && j-d >= 0 && board->getChar(i+d, j-d) == s)
            marks++;
        else
            break;
    }
    for (int d = 1; d < marksToWin; ++d) {
        if (i-d >= 0  && j+d < board->getWidth() && board->getChar(i-d, j+d) == s)
            marks++;
        else
            break;
    }

    return marks >= marksToWin;
}

/**
 * \brief   Processes the player's input.
 *
 *          The player is free to move the cursor over the board. The player's move is finished after
 *          s/he placed a mark.
 *          The game can be quit by hitting the 'q' key.
 */
void GameManager::processInput() {
    int ch;
    nodelay(stdscr, TRUE);
    if ((ch = getch()) != ERR) {
        switch(ch) {
        case 'q':
        case 'Q':
            turnFinished = true;
            isRunning = false;
            break;
        case 'k':
        case KEY_UP:
            board->moveUp();
            break;
        case 'j':
        case KEY_DOWN:
            board->moveDown();
            break;
        case 'h':
        case KEY_LEFT:
            board->moveLeft();
            break;
        case 'l':
        case KEY_RIGHT:
            board->moveRight();
            break;
        case ' ':
        case KEY_ENTER:
            if (placeMark())
                turnFinished = true;
            break;
        }
    }
    nodelay(stdscr, FALSE);
}

/**
 * \brief   Try to place a mark.
 *
 *          Try to set a mark for the current player at the current cursor location.
 *          The return value describes if the move was successful or not.
 *
 * \return  true if the move was valid, false otherwise
 */
bool GameManager::placeMark() {
    char& ch = board->getCurrentSymbol();
    if (ch == ' ') {
        ch = players[currentPlayer]->getSymbol();
        return true;
    }
    return false;
}
