#include "Board.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <ncurses.h>

using namespace std;

/**
 * \brief   Initializes a new board with empty cells (' ').
 *
 *          The created game board is of the specified size and filled with
 *          empty spaces (' ').
 *
 * \param   width   the width of the game board, must be > 0
 * \param   height  the height of the game board, must be > 0
 */
Board::Board(int width, int height) : _width(width), _height(height) {
    _board = new char*[_height];
    for (int i = 0; i < _height; ++i) {
        _board[i] = new char[_width];
        for (int j = 0; j < _width; j++) {
            _board[i][j] = ' ';
        }
    }
}

/**
 * \brief   Deconstructs this game board and frees allocated memory.
 */
Board::~Board() {
    for (int i = 0; i < _height; i++) {
        delete[] _board[i];
    }
    delete[] _board;
}

/**
 * \brief   Access the symbol at the specified position.
 *
 * \param   r   the row, must be in [0, _height]
 * \param   c   the column, must be in [0, _width]
 *
 * \return  a reference to the character at the specified position
 */
char& Board::operator()(int r, int c) {
    assert(r >= 0 && r < _height);
    assert(c >= 0 && c < _width);
    return _board[r][c];
}

int Board::getWidth() { return _width; }

int Board::getHeight() { return _height; }

/**
 * \brief   Gets a reference to the currently selected symbol.
 *
 * \return  a reference to the character at the cursor position
 */
char& Board::getCurrentSymbol() {
    return (*this)(_curs_r, _curs_c);
}

char Board::getChar(int i, int j) {
    assert(i >= 0 && i < _height);
    assert(j >= 0 && j < _width);

    return (*this)(i,j);
}

void Board::setChar(int i, int j, char c) {
    assert(i >= 0 && i < _height);
    assert(j >= 0 && j < _width);

    _board[i][j] = c;
}

/**
 * \brief   Moves the cursor to the specified position.
 *
 * \param   i   the target row, must be in [0, _height]
 * \param   j   the target column, must be in [0. _width]
 */
void Board::moveCursor(int i, int j) {
    assert(i >= 0 && i < _height);
    assert(j >= 0 && j < _width);

    _curs_r = i;
    _curs_c = j;

    draw();
}

void Board::moveUp() {
    _curs_r = ((_curs_r-1) % _height + _height) % _height;
    draw();
}

void Board::moveDown() {
    _curs_r = (_curs_r+1)%_height;
    draw();
}

void Board::moveLeft() {
    _curs_c = ((_curs_c-1) % _width + _width) % _width;
    draw();
}

void Board::moveRight() {
    _curs_c = (_curs_c+1)%_width;
    draw();
}

/**
 * \brief   Draws this board instance in the center of the screen.
 */
void Board::draw() {
    int cr, cc;
    getmaxyx(stdscr, cr, cc);
    cr /= 2; cc /= 2;

    drawFrame(cr, cc);
    drawMarkers(cr, cc);
    drawCursor(cr, cc);

    refresh();
}

/**
 * \brief   Returns the correct box drawing character for the grid position.
 *
 *          Note that the coordinates \a i and \a j are for the drawn grid, not
 *          the cells.
 *
 * \param   i   the row in the grid
 * \param   j   the column in the grid
 *
 * \return  the UTF-8 box drawing character for the specified position.
 */
string Board::getBoxCharacter(int i, int j) {
    /* top row */
    if (i == 0) {
        if (j == 0)
            return "┌";
        else if (j == 2*_width)
            return "┐";
        else if (j%2 == 0)
            return "┬";
        else
            return "─";
    /* bottom row */
    } else if (i == 2*_height) {
        if (j == 0)
            return "└";
        if (j == 2*_width)
            return "┘";
        else if (j%2 == 0)
            return "┴";
        else
            return "─";
    /* other even rows */
    } else if (i%2 == 0) {
        if (j == 0)
            return "├";
        else if (j == 2*_width)
            return "┤";
        else if (j%2 == 0)
            return "┼";
        else
            return "─";
    /* other odd rows */
    } else {
        if (j%2 == 0)
            return "│";
        else
            return " ";
    }
}

/**
 * \brief   Returns the correct box drawing character for the cursor.
 *
 *          The cursor position is highlighted using bold box drawings around
 *          the specific cell. This method returns the correct UTF8 box drawing
 *          character based on the cell's position in the grid and the relative
 *          position to the selected cell.
 *          Note that the coordinates \a i and \a j are for the drawn grid, not
 *          the cells.
 *
 * \see Board::position
 *
 * \param   i   the row in the grid
 * \param   j   the column in the grid
 * \param   p   the relative position
 *
 * \return  the bold face border string
 */
string Board::getCursorBorder(int i, int j, position p) {
    string c = getBoxCharacter(i, j);
    if (c == "┌")
        return "┏";
    else if (c == "│")
        return "┃";
    else if (c == "─")
        return "━";
    else if (c == "┐")
        return "┓";
    else if (c == "└")
        return "┗";
    else if (c == "┘")
        return "┛";
    else if (c == "┬")
        if (p == tl)
            return "┲";
        else
            return "┱";
    else if (c == "┴")
        if (p == bl)
            return "┺";
        else
            return "┹";
    else if (c == "├")
        if (p == bl)
            return "┡";
        else
            return "┢";
    else if (c == "┤")
        if (p == br)
            return "┩";
        else
            return "┪";
    else if (c == "┼")
        switch (p) {
        case tl: return "╆";
        case tr: return "╅";
        case bl: return "╄";
        default: return "╃";
        }
    else
        return "";
}

/**
 * \brief   Draws an empty frame centered around \a cr and \a cc.
 *
 *          The character dimensions for the grid are (2*_width+1) x (2*_height+1).
 *
 * \param   cr  the center row
 * \param   cc  the center column
 */
void Board::drawFrame(int cr, int cc) {
    int r, c;
    for (int i = 0; i < 2*_height + 1; ++i) {
        for (int j = 0; j < 2*_width + 1; ++j) {
            string s = getBoxCharacter(i, j);
            r = cr - _height + i;
            c = cc - _width + j;
            mvaddstr(r, c, s.c_str());
        }
    }
}

/**
 * \brief   Draws an the player's markers into the grid centered around \a cr and \a cc.
 *
 * \param   cr  the center row
 * \param   cc  the center column
 */
void Board::drawMarkers(int cr, int cc) {
    int r, c;
    for (int i = 1; i < 2*_height + 1; i+=2) {
        for (int j = 1; j < 2*_width + 1; j+=2) {
            r = cr - _height + i;
            c = cc - _width + j;
            mvprintw(r, c, "%c", _board[(i-1)/2][(j-1)/2]);
        }
    }
}

/**
 * \brief   Draws the cursor in the grid centered around \a cr and \a cc.
 *
 * \param   cr  the center row
 * \param   cc  the center column
 */
void Board::drawCursor(int c_row, int c_col) {
    int r, c, board_r, board_c;

    r = 2*_curs_r + 1;
    c = 2*_curs_c + 1;

    board_r = c_row - _height + r;
    board_c = c_col - _width + c;

    mvaddstr(board_r-1, board_c-1, getCursorBorder(r-1,c-1, tl).c_str());
    mvaddstr(board_r-1, board_c  , getCursorBorder(r-1,c  , tc).c_str());
    mvaddstr(board_r-1, board_c+1, getCursorBorder(r-1,c+1, tr).c_str());
    mvaddstr(board_r  , board_c-1, getCursorBorder(r  ,c-1, cl).c_str());
    mvaddstr(board_r  , board_c  , getCursorBorder(r  ,c  , cc).c_str());
    mvaddstr(board_r  , board_c+1, getCursorBorder(r  ,c+1, cr).c_str());
    mvaddstr(board_r+1, board_c-1, getCursorBorder(r+1,c-1, bl).c_str());
    mvaddstr(board_r+1, board_c  , getCursorBorder(r+1,c  , bc).c_str());
    mvaddstr(board_r+1, board_c+1, getCursorBorder(r+1,c+1, br).c_str());
}
