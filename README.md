

            ██████████ ██         ██████████                   ██████████                 
            ░░░░░██░░░ ░░         ░░░░░██░░░                   ░░░░░██░░░                  
                ░██     ██  █████     ░██      ██████    █████     ░██      ██████   █████ 
                ░██    ░██ ██░░░██    ░██     ░░░░░░██  ██░░░██    ░██     ██░░░░██ ██░░░██
                ░██    ░██░██  ░░     ░██      ███████ ░██  ░░     ░██    ░██   ░██░███████
                ░██    ░██░██   ██    ░██     ██░░░░██ ░██   ██    ░██    ░██   ░██░██░░░░ 
                ░██    ░██░░█████     ░██    ░░████████░░█████     ░██    ░░██████ ░░██████
                ░░     ░░  ░░░░░      ░░      ░░░░░░░░  ░░░░░      ░░      ░░░░░░   ░░░░░░ 

# about

I wrote this little TicTacToe game alongside the C++ and Games Tutorial by Ben. Check out his 
channel if you want to learn more about C++ and game development. 

[Making Games With Ben](https://www.youtube.com/user/makinggameswithben)

This little game is my contribution to the second challenge. I used ncurses for displaying and 
updating the game board, but tried to keep the dependencies and used concepts minimal.

# game

The game is variant of the well-known game of TicTacToe. You can play it with as many players as
you want to, and you can freely choose the game board size and marks required to win the game.

# build

I use CMake to build the project. As I used *ncurses* for drawing to the terminal the ncurses library
is required for building application.

```
mkdir build
cd build
cmake ..
make
```

# license

This project is licensed under the *MIT License (MIT)*.
